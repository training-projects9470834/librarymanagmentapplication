RD Training Project.
Project Description	Library Management System Spring Boot Web Application using Microservices architecture.
Implemented three separate services User, Book and Library service.

- Implemented Eureka discovery server for service discovery and Spring Cloud Gateway is used as gateway.
- Open Feign is used for Internal communication between services.
- Client-side load balancer is also being implemented.
- The three services have unit tests written for service and controller layers.

Database : MySQL
Tools : Eclipse, MySQL Workbench, Postman, Chrome Browser.
Technologies : Spring Boot framework and Spring Cloud ecosystem was used to develop the microservices.

"""Please shift to master branch to view the source code. """"